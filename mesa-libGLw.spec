Name:           mesa-libGLw
Version:        8.0.0
Release:        16
Summary:        Xt/Motif OpenGL drawing area widget library
License:        MIT
URL:            http://www.mesa3d.org
Source0:        glw-%{version}.tar.bz2

BuildRequires:  libXt-devel libGL-devel openmotif-devel

Provides:       libGLw

%description
Xt/Motif OpenGL drawing area widget library shipped by the Mesa Project.

%package devel
Summary:        Includes and more to develop MesaGLw applications
Requires:       %{name} = %{version}-%{release} libGL-devel openmotif-devel
Provides:       libGLw-devel

%description devel
This package contains all necessary include files needed
to develop applications that require these.

%prep
%autosetup -n glw-%{version} -p1

%build
%configure --disable-static --enable-motif
%make_build

%install
%make_install
%delete_la

%check

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files
%doc README
%{_libdir}/{libGLw.so.1,libGLw.so.1.0.0}

%files devel
%{_libdir}/{libGLw.so,pkgconfig/glw.pc}
%{_includedir}/GL/*.h

%changelog
* Tue Nov 27 2019 daiqianwen <daiqianwen@huawei.com> - 8.0.0-16
- Package init


